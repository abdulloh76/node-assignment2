const asyncWrapper = (requestHandler) => {
  return (req, res, next) => {
    return requestHandler(req, res, next).catch(next);
  };
};

const customErrorHandler = (error, req, res, next) => {
  res.status(500).json({message: error.message});
};

module.exports = {
  asyncWrapper,
  customErrorHandler,
};
