const mongoose = require('mongoose');

const {MONGO_USER, MONGO_PASSWORD, MONGO_HOST} = process.env;

const url = `mongodb+srv://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}?retryWrites=true&w=majority`;

module.exports.connectDatabase = () => {
  return mongoose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // useCreateIndex: true,
    // useFindAndModify: false,
  });
};
