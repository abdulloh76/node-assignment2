const jwt = require('jsonwebtoken');

module.exports.authMiddleware = (req, res, next) => {
  if (!req.headers['authorization']) {
    return res.status(400).json({
      message: 'Please provide "authorization" header',
    });
  }

  const [, token] = req.headers['authorization'].split(' ');

  if (!token) {
    return res.status(400).json({message: 'Please provide a token'});
  }

  jwt.verify(token, process.env.TOKEN_KEYWORD, (err, decoded) => {
    if (err) {
      return res.status(400).json({message: err.message});
    }
    req.user = decoded;
    next();
  });
};
