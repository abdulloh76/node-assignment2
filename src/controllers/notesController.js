const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const {asyncWrapper} = require('../utils/apiHelper');
const {authMiddleware} = require('../middleware/authMiddleware');
const {Note} = require('../models/noteModel');

router
    .get(
        '/',
        authMiddleware,
        asyncWrapper(async (req, res) => {
          const offset = +req.query.offset || 0;
          const limit = +req.query.limit || null;

          Note.find({userId: req.user._id})
              .then((notes) => {
                if (limit) notes = notes.slice(offset, offset + limit);
                else notes = notes.slice(offset);
                res.json({offset, limit, count: notes.length, notes});
              })
              .catch(() => res.status(400).json({message: 'wrong id'}));
        }),
    )
    .post(
        '/',
        authMiddleware,
        asyncWrapper(async (req, res) => {
          if (!req.body.text) {
            return res.status(400).json({message: 'text field is required'});
          }
          const note = new Note({
            userId: req.user._id,
            text: req.body.text,
          });
          await note.save();

          res.json({message: 'Success'});
        }),
    );

router
    .get(
        '/:id',
        authMiddleware,
        asyncWrapper(async (req, res) => {
          Note.findOne({_id: req.params.id})
              .then((note) => {
                if (!note) throw new Error();
                res.json({note});
              })
              .catch(() => res.status(400).json({message: 'wrong id'}));
        }),
    )
    .put(
        '/:id',
        authMiddleware,
        asyncWrapper(async (req, res) => {
          Note.findOneAndUpdate({_id: req.params.id}, {text: req.body.text})
              .then((note) => {
                if (!note) throw new Error();
                res.json({message: 'Success'});
              })
              .catch(() => res.status(400).json({message: 'wrong id'}));
        }),
    )
    .patch(
        '/:id',
        authMiddleware,
        asyncWrapper(async (req, res) => {
          Note.findOne({_id: req.params.id})
              .then((note) => {
                if (!note) throw new Error();
                note.completed = !note.completed;
                note.save();
              })
              .then(() => res.json({message: 'Success'}))
              .catch(() => res.status(400).json({message: 'wrong id'}));
        }),
    )
    .delete(
        '/:id',
        authMiddleware,
        asyncWrapper(async (req, res) => {
          Note.findOneAndRemove({_id: req.params.id})
              .then((note) => {
                if (!note) throw new Error();
                res.json({message: 'Success'});
              })
              .catch(() => res.status(400).json({message: 'wrong id'}));
        }),
    );

module.exports = {
  notesRouter: router,
};
