const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
// eslint-disable-next-line new-cap
const router = express.Router();

const {asyncWrapper} = require('../utils/apiHelper');
const {Credentials} = require('../models/credentialsModel');
const {User} = require('../models/userModel');

router.post(
    '/register',
    asyncWrapper(async (req, res) => {
      const {username, password} = req.body;

      if (!username || !password) {
        return res.status(400).json({
          message: 'username and password are both required',
        });
      }

      const credentials = new Credentials({
        username,
        password: await bcrypt.hash(password, +process.env.HASH_ROUND),
      });

      const user = new User({username});

      await credentials.save();
      await user.save();

      res.json({message: 'Success'});
    }),
);

router.post(
    '/login',
    asyncWrapper(async (req, res) => {
      const {username, password} = req.body;

      const user = await Credentials.findOne({username});

      if (!user || !(await bcrypt.compare(password, user.password))) {
        return res.status(400).json({message: 'Wrong username or password'});
      }

      const token = jwt.sign(
          {
            _id: user._id,
            username: user.username,
          },
          process.env.TOKEN_KEYWORD,
      );

      res.json({message: 'Success', jwt_token: token});
    }),
);

module.exports = {
  authRouter: router,
};
