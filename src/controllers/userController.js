const bcrypt = require('bcrypt');
const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const {asyncWrapper} = require('../utils/apiHelper');
const {authMiddleware} = require('../middleware/authMiddleware');
const {User} = require('../models/userModel');
const {Credentials} = require('../models/credentialsModel');

router
    .get('/', authMiddleware, asyncWrapper(async (req, res) => {
      const user = await User.findOne({username: req.user.username});
      if (!user) {
        return res.status(400).json({
          message: 'user profile deleting or something else',
        });
      }
      res.json(user);
    }))
    .delete('/', authMiddleware, asyncWrapper(async (req, res) => {
      await Credentials.findOneAndRemove({username: req.user.username});
      await User.findOneAndRemove({username: req.user.username});
      res.json({message: 'Success'});
    }))
    .patch('/', authMiddleware, asyncWrapper(async (req, res) => {
      const {oldPassword, newPassword} = req.body;

      const cred = await Credentials.findOne({username: req.user.username});

      if (!(await bcrypt.compare(oldPassword, cred.password))) {
        return res.status(400).json({message: 'Old password is wrong'});
      }

      await Credentials.findOneAndUpdate(
          {username: req.user.username},
          {password: await bcrypt.hash(newPassword, +process.env.HASH_ROUND)},
      );

      res.json({message: 'Success'});
    }),
    );

module.exports = {
  userRouter: router,
};
