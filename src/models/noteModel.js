const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const Note = mongoose.model('Note', mongoose.Schema({
  userId: {
    type: String,
    required: true,
    unique: false,
  },
  completed: {
    type: Boolean,
    required: true,
    default: false,
  },
  text: {
    type: String,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
}, {versionKey: false}));

module.exports = {
  Note,
};
