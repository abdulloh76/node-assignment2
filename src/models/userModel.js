const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const User = mongoose.model('User', mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
}, {versionKey: false}));

module.exports = {
  User,
};
