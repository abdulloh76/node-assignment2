const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const Credentials = mongoose.model('Credentials', mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
}, {versionKey: false}));

module.exports = {
  Credentials,
};
