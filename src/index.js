const morgan = require('morgan');
const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors');
const app = express();
dotenv.config();

const {connectDatabase} = require('./utils/connectMongo');
const {customErrorHandler} = require('./utils/apiHelper');
const {authRouter} = require('./controllers/authController');
const {userRouter} = require('./controllers/userController');
const {notesRouter} = require('./controllers/notesController');

const PORT = process.env.PORT;

app.use(cors());
app.use(morgan('tiny'));
app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/notes', notesRouter);
app.use(customErrorHandler);

const start = async () => {
  try {
    await connectDatabase();

    app.listen(PORT, () => {
      console.log('Server launched!');
    });
  } catch (err) {
    console.error('Served launch failed: ', err.message);
  }
};

start();
